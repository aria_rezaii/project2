from django.shortcuts import render
from shop.models import Product, Category


def product(request):
    prod = Product.objects.filter(id=2)[0]
    cats = Category.objects.all()
    context = {'product': prod, 'categories': cats}
    return render(request, 'product.html', context)
