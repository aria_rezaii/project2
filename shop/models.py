from django.db import models

class Category(models.Model):
    name = models.CharField(max_length=30)
    sup_cat = models.ForeignKey("self", blank=True,null=True)
    def __str__(self):
        return self.name

class Product(models.Model):
    name = models.CharField(max_length=30)
    creator = models.CharField(max_length=30)
    publisher = models.CharField(max_length=30)
    category = models.ForeignKey(Category)
    price = models.DecimalField(max_digits=6,decimal_places=2)
    number = models.DecimalField(max_digits=6,decimal_places=0)
    small_image = models.FilePathField(path='./shop/static/images/tmb', null=True, blank=True)
    large_image = models.FilePathField(path='./shop/static/images/lg', null=True, blank=True)
    availability = models.BooleanField()
    def __str__(self):
        return self.name