from django.conf.urls import patterns, include, url

from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'final.views.home', name='home'),
    url(r'^shop/', include('shop.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
